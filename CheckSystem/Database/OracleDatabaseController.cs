﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OracleClient;
using System.Data;
using System.Configuration;
using CheckSystem.Extension;

namespace Database
{
    class OracleDatabaseController : ILog
    {
        private OracleConnection _con;
        private OracleCommand _cmd;

        public OracleDatabaseController()
        {
            try
            {
                this._con = new OracleConnection();
                string host = ConfigurationManager.AppSettings["HOST"].ToString();
                string port = ConfigurationManager.AppSettings["PORT"].ToString();
                string serviceName = ConfigurationManager.AppSettings["SERVICE_NAME"].ToString();
                string ID = ConfigurationManager.AppSettings["ID"].ToString();
                string pass = ConfigurationManager.AppSettings["PASS"].ToString();
                string strConnection = "DATA SOURCE=" +
                    "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = " + host + ")(PORT = " +  port + "))" +
                    "(CONNECT_DATA = (SERVICE_NAME = " + serviceName  + ")));USER ID=" + ID + ";Password="+ pass +"";
                this._con.ConnectionString = strConnection;
                this._con.Open();
            }
            catch (Exception ex)
            {
                this.ErrorLog("DB接続に失敗しました。", ex);
                throw new ApplicationException(ex.Message);
            }
        }

        public DataTable Reader(string query, object[] argument)
        {
            try
            {
                this._cmd = new OracleCommand(query, this._con);
                if (argument != null)
                {
                    this.SetParameter(argument);
                }
                this._cmd.CommandType = CommandType.Text;
                OracleDataAdapter oAdpt = new OracleDataAdapter(this._cmd);
                DataTable dt = new DataTable();
                oAdpt.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                this.ErrorLog("データの取得に失敗しました。", ex);
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                this._con.Close();
            }
        }

        private void SetParameter(object[] argument)
        {
            if (this._cmd != null)
                this._cmd.Parameters.Clear();
            if (argument == null) return;

            foreach (object arg in argument)
            {
                object[] argList = arg.ToString().Split(',');
                OracleParameter parameter = this._cmd.CreateParameter();
                if (argList == null)
                {
                    parameter.Value = (null);
                }
                else
                {
                    parameter.ParameterName = argList[0].ToString();
                    try
                    {
                        parameter.DbType = this.GetDbType(argList[1]);
                    }
                    catch (ArgumentException ex)
                    {
                        this.ErrorLog("失敗しました。", ex);
                        throw ex;
                    }
                    parameter.Value = argList[1];
                }
                this._cmd.Parameters.Add(parameter);

            }
            this._cmd.Prepare();


        }

        private DbType GetDbType(object arg)
        {
            if (arg is int) return DbType.Int32;
            else if (arg is float || arg is double) return DbType.Double;
            else if (arg is string) return DbType.String;
            else if (arg is DateTime) return DbType.DateTime;
            else if (arg is bool) return DbType.Boolean;
            else throw new ArgumentException("渡された引数の型が処理できない形式です。");

        }
    }
}
