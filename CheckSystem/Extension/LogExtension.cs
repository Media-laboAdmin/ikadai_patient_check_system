﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;

namespace CheckSystem.Extension
{
    /// <summary>
    /// ログ機能インターフェース
    /// </summary>
    public interface ILog
    {
    }

    /// <summary>
    /// ログ機能エクステンション
    /// </summary>
    public static class LogExtension
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// ログメッセージを生成する
        /// </summary>
        /// <pre>
        /// 呼び出し元のクラス名、メソッド名をスタックフレームから
        /// 取り出し、ログメッセージを生成する。
        /// ログメッセージフォーマット : ClassName.MethodName() : message....
        /// </pre>
        /// <param name="caller">呼び出し元クラスのインスタンス</param>
        /// <param name="logMessage">ログメッセージ</param>
        /// <returns></returns>
        [DynamicSecurityMethod] // DebugビルドとReleaseビルドでログに出力されるメソッド名がずれるのを防ぐおまじない
        private static string BuildLogMessage(ILog caller, string logMessage)
        {
            StackFrame callerStackFrame = new StackFrame(2);
            //MethodBase callerMethod = callerFrame.GetMethod();
            string callerClassName = caller.GetType().Name;
            string callerMethodName = callerStackFrame.GetMethod().Name;
            string log = string.Format("{0}.{1}() : {2}", callerClassName, callerMethodName, logMessage);
            return log;
            //StringBuilder builder = new StringBuilder();
            //builder.Append(callerClassName);
            //builder.Append(".");
            //builder.Append(callerMethodName);
            //builder.Append(" ");
            //builder.Append(logMessage);
            //return builder.ToString();
        }

        /// <summary>
        /// Debugログを出力する
        /// </summary>
        /// <param name="caller">このメソッドを呼び出したインスタンス</param>
        /// <param name="logMessage">ログメッセージ</param>
        [DynamicSecurityMethod] // DebugビルドとReleaseビルドでログに出力されるメソッド名がずれるのを防ぐおまじない
        public static void DebugLog(this ILog caller, string logMessage)
        {
            logger.Debug(BuildLogMessage(caller, logMessage));
        }

        /// <summary>
        /// Informationログを出力する
        /// </summary>
        /// <param name="caller">このメソッドを呼び出したインスタンス</param>
        /// <param name="logMessage">ログメッセージ</param>
        [DynamicSecurityMethod] // DebugビルドとReleaseビルドでログに出力されるメソッド名がずれるのを防ぐおまじない
        public static void InfoLog(this ILog caller, string logMessage)
        {
            logger.Info(BuildLogMessage(caller, logMessage));
        }

        /// <summary>
        /// Warnningログを出力する
        /// </summary>
        /// <param name="caller">このメソッドを呼び出したインスタンス</param>
        /// <param name="logMessage">ログメッセージ</param>
        [DynamicSecurityMethod] // DebugビルドとReleaseビルドでログに出力されるメソッド名がずれるのを防ぐおまじない
        public static void WarnLog(this ILog caller, string logMessage)
        {
            logger.Warn(BuildLogMessage(caller, logMessage));
        }

        /// <summary>
        /// Errorログを出力する
        /// </summary>
        /// <param name="caller">このメソッドを呼び出したインスタンス</param>
        /// <param name="logMessage">ログメッセージ</param>
        [DynamicSecurityMethod] // DebugビルドとReleaseビルドでログに出力されるメソッド名がずれるのを防ぐおまじない
        public static void ErrorLog(this ILog caller, string logMessage)
        {
            logger.Error(BuildLogMessage(caller, logMessage));
        }
        /// <summary>
        /// Errorログを出力する
        /// </summary>
        /// <param name="caller">このメソッドを呼び出したインスタンス</param>
        /// <param name="logMessage">ログメッセージ</param>
        /// <param name="ex">例外オブジェクト</param>
        [DynamicSecurityMethod] // DebugビルドとReleaseビルドでログに出力されるメソッド名がずれるのを防ぐおまじない
        public static void ErrorLog(this ILog caller, string logMessage, Exception ex)
        {
            logger.Error(BuildLogMessage(caller, logMessage), ex);
        }

        /// <summary>
        /// Fatalログを出力する
        /// </summary>
        /// <param name="caller">このメソッドを呼び出したインスタンス</param>
        /// <param name="logMessage">ログメッセージ</param>
        [DynamicSecurityMethod] // DebugビルドとReleaseビルドでログに出力されるメソッド名がずれるのを防ぐおまじない
        public static void FatalLog(this ILog caller, string logMessage)
        {
            logger.Fatal(BuildLogMessage(caller, logMessage));
        }
        /// <summary>
        /// Fatalログを出力する
        /// </summary>
        /// <param name="caller">このメソッドを呼び出したインスタンス</param>
        /// <param name="logMessage">ログメッセージ</param>        /// <param name="ex">例外オブジェクト</param>
        [DynamicSecurityMethod] // DebugビルドとReleaseビルドでログに出力されるメソッド名がずれるのを防ぐおまじない
        public static void FatalLog(this ILog caller, string logMessage, Exception ex)
        {
            logger.Fatal(BuildLogMessage(caller, logMessage), ex);
        }
    }
}

// DebugビルドとReleaseビルドでログに出力されるメソッド名がずれるのを防ぐおまじない
//   StackFrame クラス (System.Diagnostics) を使用すると、呼び出し元メソッドを取得することができる。
//   しかし、Release ビルドされたアセンブリでは JIT 最適化により呼び出し元メソッドがインライン化されている可能性があり、
//   スキップするスタック上のフレーム数を決め打ちで2としている本クラスのBuildLogMessageでは
//   メソッド名がずれたり、最悪GetMethodの取得値がnullとなるためNull参照例外が発生する。
//   メソッドに DynamicSecurityMethodAttribute クラス (System.Security) を属性として付加することにより、
//   呼び出し元メソッドのコールスタックを維持することができる。
//   このクラスは mscorlib.dll の internal クラスなので、コールスタック維持のためには
//   同名のクラスを自前で用意する必要がある。
namespace System.Security
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    internal sealed class DynamicSecurityMethodAttribute : Attribute
    {
    }
}