﻿using Database;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Configuration;
using CheckSystem.MatchSearch;
using CheckSystem.Extension;
using System.ComponentModel;

namespace CheckSystem.MatchSearch
{
    public partial class MatchSearchForm : Form, ILog
    {
        RKKTAccess rkkt = new RKKTAccess();

        int MatchCount;
        int RowNum;
        int NameNum;
        int BirthNum;
        int AddrNum;
        int TelNum;
        // データ受け渡し用変数
        List<PatientInfo> dataList = new List<PatientInfo>();
        List<PatientInfo> unmatchDataList = new List<PatientInfo>();
        List<PatientInfo> displayDataList = new List<PatientInfo>();
        bool checkboxFlg = false;
        OpenFileDialog ofd;
        public MatchSearchForm()
        {
            InitializeComponent();

            this.InfoLog("プログラム開始");

            // ComboBox用データ作成 //ListでOK //IList インターフェイスまたは IListSource インターフェイスを実装する、DataSet または Array などのオブジェクト。
            List<ItemSet> src = new List<ItemSet>();
            // パターンの選択肢をconfigファイルから取得、カンマで切り分ける
            string[] patternes = ConfigurationManager.AppSettings["ALL"].Split(',');
            
            foreach (var patterne in patternes)
            {
                // コンボボックスのラベル取得用
                string[] patterneData = ConfigurationManager.AppSettings[patterne].Split(',');
                src.Add(new ItemSet(patterne, patterneData[5]));
            }

            // ComboBoxに表示と値をセット
            PattarneSelect.DataSource = src;
            PattarneSelect.DisplayMember = "Label";
            PattarneSelect.ValueMember = "Value";

            // 初期値セット
            PattarneSelect.SelectedIndex = 0;
            PattarneSelect_SelectedIndexChanged(null, null);

            Form1_Load();
        }
        /// <summary>
        /// フォーム読み込み時
        /// </summary>
        private void Form1_Load()
        {
            this.dataGridView.AutoGenerateColumns = true;
            this.dataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dataGridView.ColumnHeadersHeight = this.dataGridView.ColumnHeadersHeight * 2;
            this.dataGridView.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            this.dataGridView.CellPainting += new DataGridViewCellPaintingEventHandler(dataGridView_CellPainting);
            this.dataGridView.Paint += new PaintEventHandler(dataGridView_Paint);
        }
        /// <summary>
        /// dataGridViewの見た目作成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void dataGridView_Paint(object sender, PaintEventArgs e)
        {
            string[] headers = { "名前", "生年月日", "", "住所", "電話番号" };
            this.dataGridView.Columns["ExcelName"].DisplayIndex = 0;
            this.dataGridView.Columns["HospitalName"].DisplayIndex = 1;
            this.dataGridView.Columns["ExcelBirth"].DisplayIndex = 2;
            this.dataGridView.Columns["HospitalBirth"].DisplayIndex = 3;
            this.dataGridView.Columns["Match"].DisplayIndex = 4;
            this.dataGridView.Columns["space"].DisplayIndex = 5;
            this.dataGridView.Columns["ExcelAddr"].DisplayIndex = 6;
            this.dataGridView.Columns["HospitalAddr"].DisplayIndex = 7;
            this.dataGridView.Columns["ExcelTel"].DisplayIndex = 8;
            this.dataGridView.Columns["HospitalTel"].DisplayIndex = 9;
            for (int j = 0; j < 10;)
            {
                Rectangle r1 = this.dataGridView.GetCellDisplayRectangle(j, -1, true); //get the column header cell
                // 2回目以降のグリッド描画時にヘッダーの順番が勝手に変わってしまうため、ごり押しで初回の位置を持たせています。
                switch (j)
                {
                    case 0:
                        r1.X = 42;
                        break;
                    case 2:
                        r1.X = 342;
                        break;
                    case 4:
                        r1.X = 642;
                        break;
                    case 6:
                        r1.X = 792;
                        break;
                    case 8:
                        r1.X = 1092;
                        break;
                }

                r1.X += 1;
                r1.Y += 1;
                r1.Width = r1.Width * 2 - 2;
                r1.Height = r1.Height / 2 - 2;
                e.Graphics.FillRectangle(new SolidBrush(this.dataGridView.ColumnHeadersDefaultCellStyle.BackColor), r1);
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString(headers[j / 2],
                    this.dataGridView.ColumnHeadersDefaultCellStyle.Font,
                    new SolidBrush(this.dataGridView.ColumnHeadersDefaultCellStyle.ForeColor),
                    r1,
                    format);
                j += 2;
            }
        }
        /// <summary>
        /// dataGridViewの見た目作成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void dataGridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1 && e.ColumnIndex > -1)
            {
                e.PaintBackground(e.CellBounds, false);
                Rectangle r2 = e.CellBounds;
                r2.Y += e.CellBounds.Height / 2;
                r2.Height = e.CellBounds.Height / 2;
                e.PaintContent(r2);
                e.Handled = true;
            }
        }

        /// <summary>
        /// パターンの選択肢のラベルとバリュー用
        /// </summary>
        public class ItemSet
        {
            public String Label { get; set; }
            public String Value { get; set; }
            
            public ItemSet(String v, String l)
            {
                Label = l;
                Value = v;
            }
        }
        /// <summary>
        /// エクセル取込ボタン押下　エクセルの取込と病院情報の取得
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ExcelRead_Click(object sender, EventArgs e)
        {
            this.InfoLog("Excel読込開始");
            MatchCount = 0;
            // 選択されているバリューでパターンの情報を取得、カンマで切り分ける
            string[] ExcelPositions = ConfigurationManager.AppSettings[PattarneSelect.SelectedValue.ToString()].Split(',');
            // 行番号、氏名列番号、生年月日列番号、住所列番号、電話番号列番号にそれぞれ入れる
            RowNum = int.Parse(ExcelPositions[0]);
            NameNum = int.Parse(ExcelPositions[1]);
            BirthNum = int.Parse(ExcelPositions[2]);
            AddrNum = int.Parse(ExcelPositions[3]);
            TelNum = int.Parse(ExcelPositions[4]);
            
            //OpenFileDialogクラスのインスタンスを作成
            ofd = new OpenFileDialog();
            //はじめのファイル名を指定する
            ofd.FileName = "";
            //はじめに表示されるフォルダを指定する
            ofd.InitialDirectory = @ConfigurationManager.AppSettings["FOLDERPATH"];
            //[ファイルの種類]に表示される選択肢を指定する
            //指定しないとすべてのファイルが表示される
            ofd.Filter = "Excelファイル(*.xls;*.xlsx)|*.xls;*.xlsx";
            //[ファイルの種類]ではじめに選択されるものを指定する
            //2番目の「すべてのファイル」が選択されているようにする
            ofd.FilterIndex = 2;
            //タイトルを設定する
            ofd.Title = "開くファイルを選択してください";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            ofd.RestoreDirectory = true;
            //存在しないファイルの名前が指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            ofd.CheckFileExists = true;
            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            ofd.CheckPathExists = true;

            //ダイアログを表示する
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //ProgressDialogオブジェクトを作成する
                //病院情報の取得
                ProgressDialog pd = new ProgressDialog("処理中....",
                    new DoWorkEventHandler(PatientInfoReading));
                //進行状況ダイアログを表示する
                DialogResult dialogResult = pd.ShowDialog(this);
                //結果を取得する
                if (dialogResult == DialogResult.Abort)
                {
                    //エラー情報を取得する
                    Exception ex = pd.Error;
                    this.ErrorLog("エクセル取込エラー: " + ex.Message);
                }
                else if (dialogResult == DialogResult.OK)
                {
                    //結果を取得する
                    this.InfoLog("エクセル取込成功");
                    if (dataList.Count > 0)
                    {
                        // エクセルと病院登録のデータをすべて取得後にグリッドに表示
                        dataGridView.DataSource = dataList;
                        drawingList();

                        MatchNumLabel.Text = MatchCount + "名";
                        UnmatchNumLabel.Text = (dataList.Count - MatchCount) + "名";
                        // カルテIDとMatchFlgは非表示
                        this.dataGridView.Columns[10].Visible = false;
                        this.dataGridView.Columns[11].Visible = false;
                    }
                }
                else
                {
                    this.ErrorLog("予想外のDialogResult : " + dialogResult.ToString());
                    MessageBox.Show("予想外のDialogResult : " + dialogResult.ToString());
                }
                
                //後始末
                pd.Dispose();
            }
        }
        /// <summary>
        /// DoWorkイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PatientInfoReading(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = (BackgroundWorker)sender;
            unmatchDataList = new List<PatientInfo>();
            displayDataList = new List<PatientInfo>();

            //パラメータを取得する
            bw.ReportProgress(0, "エクセル取込中");
            try
            {
                #region 初期設定と宣言
                // Excelのファイルパス
                string readPath = ofd.FileName;
                // ファイル取得用
                FileStream fs = null;
                // ブック格納用
                IWorkbook readBook = null;
                #endregion

                #region ファイル読み込み
                // 読込用Excelファイルの読込
                using (fs = File.OpenRead(readPath))
                {
                    readBook = WorkbookFactory.Create(fs, ImportOption.All);
                }
                #endregion

                #region データのコピー
                // データ受け渡し用変数
                dataList = new List<PatientInfo>();
                // Excelデータをシートごとに読み取り
                for (int i = 0; i < readBook.NumberOfSheets; i++)
                {
                    ISheet sheet = readBook.GetSheetAt(i);

                    for (int rowCount = RowNum; rowCount <= sheet.LastRowNum; rowCount++)
                    {
                        PatientInfo data = new PatientInfo();
                        // 行データ取得
                        IRow row = sheet.GetRow(rowCount);

                        if (row.GetCell(NameNum) != null && row.GetCell(NameNum).StringCellValue != "")
                        {
                            data.ExcelName = row.GetCell(NameNum).StringCellValue;
                        }
                        else
                        {
                            data.ExcelName = "入力なし";
                        }
                        if (row.GetCell(BirthNum) != null)
                        {
                            if (row.GetCell(BirthNum).CellType == CellType.Numeric && ((DateTime)row.GetCell(BirthNum).DateCellValue).ToString() != "")
                            {
                                data.ExcelBirth = ((DateTime)row.GetCell(BirthNum).DateCellValue).ToString("yyyy/MM/dd");
                            }
                            else
                            if (row.GetCell(BirthNum).CellType == CellType.String && row.GetCell(BirthNum).StringCellValue != "")
                            {
                                data.ExcelBirth = row.GetCell(BirthNum).StringCellValue;
                            }
                        }
                        else
                        {
                            // 病院情報取得時に未入力によるエラーを回避するため
                            data.ExcelBirth = "9999/12/31";
                        }

                        // 名前と生年月日が未入力の場合はリストに入れない。
                        if (data.ExcelName.Equals("入力なし") && data.ExcelBirth.Equals("-1"))
                        { 
                            continue;
                        }

                        // パターンによってはなし(-1)のため
                        if (TelNum >= 0 && row.GetCell(TelNum) != null)
                        {
                            if (row.GetCell(TelNum).CellType == CellType.Numeric && row.GetCell(TelNum).NumericCellValue.ToString() != "")
                            {
                                data.ExcelTel = row.GetCell(TelNum).NumericCellValue.ToString();
                            }
                            else
                            if (row.GetCell(TelNum).CellType == CellType.String && row.GetCell(TelNum).StringCellValue != "")
                            {
                                data.ExcelTel = row.GetCell(TelNum).StringCellValue;
                            }
                            else
                            {
                                data.ExcelTel = "-1";
                            }
                        }
                        else
                        {
                            data.ExcelTel = "-1";
                        }

                        // パターンによってはなし(-1)のため
                        if (AddrNum >= 0 && row.GetCell(AddrNum) != null)
                        {
                            data.ExcelAddr = row.GetCell(AddrNum).StringCellValue;
                        }
                        else
                        {
                            data.ExcelAddr = "入力なし";
                        }
                        dataList.Add(data);
                    }
                }
                //ProgressChangedイベントハンドラを呼び出し、
                //コントロールの表示を変更する
                bw.ReportProgress(50, "エクセル取込終了");
                this.InfoLog("取込終了/病院登録データの検索開始");
                // プログレスバーの数字用
                int par = 50 / dataList.Count;
                int totalProgress = 50;
                // 読み取ったデータ
                for (int i = 0; i < dataList.Count; i++)
                {
                    // Perfect: 名前、生年月日、住所、電話番号すべてで一致確認
                    DataTable result = rkkt.getHospitalInfo(dataList[i].ExcelName, dataList[i].ExcelBirth, dataList[i].ExcelAddr, dataList[i].ExcelTel, SearchMode.Perfect);
                    if( result == null)
                    {
                        break;
                    }
                    else
                    {
                        if (result.Rows.Count > 0)
                        {
                            // 一致患者様数カウントアップ
                            MatchCount++;
                            dataList[i].KID = result.Rows[0]["YOBITEXT9"].ToString();
                            dataList[i].HospitalName = result.Rows[0]["PNAME"].ToString();
                            dataList[i].HospitalBirth = ((DateTime)result.Rows[0]["BIRTH"]).ToString("yyyy/MM/dd");
                            dataList[i].HospitalAddr = result.Rows[0]["PADDR1"].ToString() + result.Rows[0]["PADDR2"].ToString();
                            dataList[i].HospitalTel = result.Rows[0]["JITAKTEL"].ToString();
                            dataList[i].MatchFlg = true;
                        }
                        else
                        {
                            // Both: 名前、生年月日の両方で一致確認
                            result = rkkt.getHospitalInfo(dataList[i].ExcelName, dataList[i].ExcelBirth, dataList[i].ExcelAddr, dataList[i].ExcelTel, SearchMode.Both);
                            if (result.Rows.Count > 0)
                            {
                                dataList[i].KID = result.Rows[0]["YOBITEXT9"].ToString();
                                dataList[i].HospitalName = result.Rows[0]["PNAME"].ToString();
                                dataList[i].HospitalBirth = ((DateTime)result.Rows[0]["BIRTH"]).ToString("yyyy/MM/dd");
                                dataList[i].HospitalAddr = result.Rows[0]["PADDR1"].ToString() + result.Rows[0]["PADDR2"].ToString();
                                dataList[i].HospitalTel = result.Rows[0]["JITAKTEL"].ToString();
                                if (AddrNum == -1 && TelNum == -1)
                                {
                                    // 住所と電話番号が記載されていないフォーマットの時は名前と生年月日が一致すれば一致患者様数カウントアップ
                                    MatchCount++;
                                    dataList[i].MatchFlg = true;
                                }
                                else
                                {
                                    dataList[i].MatchFlg = false;
                                }
                            }
                            else
                            {
                                // Either: 名前、生年月日で一致確認、どっちかがいっちすればいい
                                result = rkkt.getHospitalInfo(dataList[i].ExcelName, dataList[i].ExcelBirth, dataList[i].ExcelAddr, dataList[i].ExcelTel, SearchMode.Either);
                                if (result.Rows.Count > 0)
                                {
                                    dataList[i].KID = result.Rows[0]["YOBITEXT9"].ToString();
                                    dataList[i].HospitalName = result.Rows[0]["PNAME"].ToString();
                                    dataList[i].HospitalBirth = ((DateTime)result.Rows[0]["BIRTH"]).ToString("yyyy/MM/dd");
                                    dataList[i].HospitalAddr = result.Rows[0]["PADDR1"].ToString() + result.Rows[0]["PADDR2"].ToString();
                                    dataList[i].HospitalTel = result.Rows[0]["JITAKTEL"].ToString();
                                    dataList[i].MatchFlg = false;
                                }
                                else
                                {
                                    // 全く該当する患者様がいないとき
                                    dataList[i].KID = "該当なし";
                                    dataList[i].HospitalName = "該当なし";
                                    dataList[i].HospitalBirth = "該当なし";
                                    dataList[i].HospitalAddr = "該当なし";
                                    dataList[i].HospitalTel = "該当なし";
                                    dataList[i].MatchFlg = false;
                                }
                            }
                        }
                    }

                    // 生年月日が未入力だった場合は”入力なし”に変更
                    if (dataList[i].ExcelBirth.Equals("9999/12/31"))
                    {
                        dataList[i].ExcelBirth = "入力なし";
                    }
                    // 電話番号を入力しないフォーマットの場合は”入力なし”に変更
                    if (dataList[i].ExcelTel.Equals("-1"))
                    {
                        dataList[i].ExcelTel = "入力なし";
                    }
                    // 不一致のデータは不一致のみ表示用のリストにも入れる
                    if (!dataList[i].MatchFlg)
                    {
                        unmatchDataList.Add(dataList[i]);
                    }
                    // ％を更新
                    totalProgress += par;
                    bw.ReportProgress(totalProgress, "病院情報取得中");
                }
                this.InfoLog("病院登録情報の検索終了");
                bw.ReportProgress(100, "病院情報取得終了");
                #endregion
            }
            catch (Exception ex)
            {
                this.ErrorLog("エクセルの取込に失敗しました。", ex);
                throw;
            }
            finally
            {
                this.InfoLog("処理終了");
            }
        }
        /// <summary>
        /// dataGridViewの不一致項目の色付けと一致ボタンの描画
        /// </summary>
        private void drawingList()
        {
            if (checkboxFlg)
            {
                displayDataList = unmatchDataList;
            }
            else
            {
                displayDataList = dataList;
            }
            // 不一致項目に色付けしていく, 一致不一致のボタンも表示
            for (int i = 0; i < displayDataList.Count; i++)
            {
                DataGridViewButtonCell buttonCell = (DataGridViewButtonCell)dataGridView.Rows[i].Cells["Match"];
                if (displayDataList[i].MatchFlg)
                {
                    // ○ボタン
                    buttonCell.Value = "○";
                }
                else
                {
                    // ×ボタン
                    buttonCell.Value = "×";
                }

                // 名前の不一致項目色付け
                if (dataGridView.Rows[i].Cells["ExcelName"].Value.ToString() != dataGridView.Rows[i].Cells["HospitalName"].Value.ToString())
                {
                    dataGridView.Rows[i].Cells["ExcelName"].Style.BackColor = Color.LightPink;
                    dataGridView.Rows[i].Cells["HospitalName"].Style.BackColor = Color.LightPink;
                }
                else
                {
                    dataGridView.Rows[i].Cells["ExcelName"].Style.BackColor = Color.White;
                    dataGridView.Rows[i].Cells["HospitalName"].Style.BackColor = Color.White;
                }
                // 生年月日の不一致項目色付け　病院登録情報は日時を切り取って比較
                if (dataGridView.Rows[i].Cells["HospitalBirth"].Value.ToString().Equals("なし") || dataGridView.Rows[i].Cells["HospitalBirth"].Value.ToString().Equals("該当なし"))
                {
                    dataGridView.Rows[i].Cells["ExcelBirth"].Style.BackColor = Color.LightPink;
                    dataGridView.Rows[i].Cells["HospitalBirth"].Style.BackColor = Color.LightPink;
                }
                else 
                if (dataGridView.Rows[i].Cells["ExcelBirth"].Value.ToString() != dataGridView.Rows[i].Cells["HospitalBirth"].Value.ToString().Substring(0, 10))
                {
                        dataGridView.Rows[i].Cells["ExcelBirth"].Style.BackColor = Color.LightPink;
                        dataGridView.Rows[i].Cells["HospitalBirth"].Style.BackColor = Color.LightPink;
                }
                else
                {
                    dataGridView.Rows[i].Cells["ExcelBirth"].Style.BackColor = Color.White;
                    dataGridView.Rows[i].Cells["HospitalBirth"].Style.BackColor = Color.White;
                }
                
                // 住所の不一致項目色付け パターンによって入力されていない場合は色付けしない。
                if (dataGridView.Rows[i].Cells["ExcelAddr"].Value.ToString() != dataGridView.Rows[i].Cells["HospitalAddr"].Value.ToString() && AddrNum != -1)
                {
                    dataGridView.Rows[i].Cells["ExcelAddr"].Style.BackColor = Color.LightPink;
                    dataGridView.Rows[i].Cells["HospitalAddr"].Style.BackColor = Color.LightPink;
                }
                else
                {
                    dataGridView.Rows[i].Cells["ExcelAddr"].Style.BackColor = Color.White;
                    dataGridView.Rows[i].Cells["HospitalAddr"].Style.BackColor = Color.White;
                }
                // 電話の不一致項目色付け パターンによって入力されていない場合は色付けしない。
                if (dataGridView.Rows[i].Cells["ExcelTel"].Value.ToString() != dataGridView.Rows[i].Cells["HospitalTel"].Value.ToString() && TelNum != -1)
                {
                    dataGridView.Rows[i].Cells["ExcelTel"].Style.BackColor = Color.LightPink;
                    dataGridView.Rows[i].Cells["HospitalTel"].Style.BackColor = Color.LightPink;
                }
                else
                {
                    dataGridView.Rows[i].Cells["ExcelTel"].Style.BackColor = Color.White;
                    dataGridView.Rows[i].Cells["HospitalTel"].Style.BackColor = Color.White;
                }
            }
        }
        /// <summary>
        /// labelに現在コンボ選択の内容を表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PattarneSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItemSet tmp = ((ItemSet)PattarneSelect.SelectedItem);
        }
        /// <summary>
        /// 不一致の患者様を絞り込む
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefineCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (RefineCheckBox.Checked)
            {
                checkboxFlg = true;
                dataGridView.DataSource = unmatchDataList;
                drawingList();
            } else
            {
                checkboxFlg = false;
                dataGridView.DataSource = dataList;
                drawingList();
            }
        }
        /// <summary>
        /// CSVボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void outputButton_Click(object sender, EventArgs e)
        {
            this.InfoLog("CSV出力開始");
            try
            {
                // appendをtrueにすると，既存のファイルに追記
                //         falseにすると，ファイルを新規作成する
                var append = false;
                DateTime now = DateTime.Now;
                string path = @ConfigurationManager.AppSettings["OUTPUTPATH"];
                string fileName = "健診受診者照合_" + now.ToString("yyyyMMddHHmmss")+".csv";
                string file = Path.Combine(path, fileName);
                // 出力用のファイルを開く
                using (var sw = new StreamWriter(file, append, System.Text.Encoding.GetEncoding("sjis")))
                {
                    for (int i = 0; i < displayDataList.Count; ++i)
                    {
                        // カルテ番号、名前、生年月日、住所、電話番号
                        sw.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}", 
                            displayDataList[i].KID, displayDataList[i].ExcelName, 
                            displayDataList[i].HospitalName, 
                            displayDataList[i].ExcelBirth, 
                            displayDataList[i].HospitalBirth, 
                            displayDataList[i].ExcelAddr, 
                            displayDataList[i].HospitalAddr, 
                            displayDataList[i].ExcelTel, 
                            displayDataList[i].HospitalTel
                            );
                    }
                }
                if(path == "")
                {
                    path = Directory.GetCurrentDirectory();
                }
                MessageBox.Show("CSV出力に成功しました。\r\n出力先\r\n" + path);
            }
            catch (Exception ex)
            {
                // ファイルを開くのに失敗したときエラーメッセージを表示
                this.ErrorLog("CSV出力に失敗しました。" , ex);
                MessageBox.Show(ex.Message);
            }
        }

        int selectIndex;
        /// <summary>
        /// 各行の○×ボタン押下時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            selectIndex = e.RowIndex;
            // 選択した行がヘッダ以外　かつ　クリックした箇所が一致ボタンのとき
            // エクセル読み込んでリストを描画した際に、一致ボタンのcolumIndexが０になっているため０を指定。
            if (selectIndex >= 0 && e.ColumnIndex == 0)
            {
                try
                {
                    CandidatePatientForm candidatePatient =
                new CandidatePatientForm(this,
                displayDataList[selectIndex].ExcelName,
                displayDataList[selectIndex].ExcelBirth,
                displayDataList[selectIndex].ExcelAddr,
                displayDataList[selectIndex].ExcelTel);
                    candidatePatient.Owner = this;
                    candidatePatient.Show();
                    this.Enabled = false;
                }
                catch (Exception ex)
                {
                    this.ErrorLog("条件適合画面の表示に失敗しました。", ex);
                    MessageBox.Show(ex.Message);
                }
            }
        }
        /// <summary>
        /// 閉じるボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeButton_Click(object sender, EventArgs e)
        {
            this.InfoLog("プログラム終了");
            this.Close();
        }
        /// <summary>
        /// 病院登録情報の表示を変更
        /// </summary>
        /// <param name="KID"></param>
        /// <param name="Name"></param>
        /// <param name="Birth"></param>
        /// <param name="Addr"></param>
        /// <param name="Tel"></param>
        public void changeDisplayInfo (string KID, string Name, string Birth, string Addr, string Tel)
        {
            displayDataList[selectIndex].KID = KID;
            displayDataList[selectIndex].HospitalName = Name;
            displayDataList[selectIndex].HospitalBirth = Birth;
            displayDataList[selectIndex].HospitalAddr = Addr;
            displayDataList[selectIndex].HospitalTel = Tel;
            drawingList();
        }
    }
}