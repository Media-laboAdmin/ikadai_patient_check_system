﻿using CheckSystem.Extension;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CheckSystem.MatchSearch
{
    public partial class CandidatePatientForm : Form, ILog
    {
        MatchSearchForm matchSearch;
        RKKTAccess rkkt = new RKKTAccess();
        List<PatientInfo> dataList = new List<PatientInfo>();
        DataTable patientInfoList;

        public CandidatePatientForm(MatchSearchForm matchSearchForm, string Name, string Birth, string Addr, string Tel)
        {
            InitializeComponent();
            this.InfoLog("患者履歴一致検索画面開始");

            NameLabel.Text = Name;
            BirthLabel.Text = Birth;
            TelLabel.Text = Tel;
            AddrLabel.Text = Addr;
            matchSearch = matchSearchForm;
            dataList = new List<PatientInfo>();

            // Either: 名前、生年月日で一致確認、どっちかがいっちすればいい
            this.InfoLog("病院登録情報取得開始");
            try
            {
                patientInfoList = rkkt.getHospitalInfo(Name, Birth, Addr, Tel, SearchMode.Either);
            }
            catch (Exception ex)
            {
                this.ErrorLog("登録情報の取得に失敗しました。", ex);
                MessageBox.Show(ex.Message);
            }
            this.InfoLog("病院登録情報取得終了");
            
            drawingList();
        }
        /// <summary>
        /// dataGridViewに表示
        /// </summary>
        public void drawingList()
        {
            dataList = new List<PatientInfo>();
            if (patientInfoList.Rows.Count > 0)
            {
                int index = 0;
                for (int i = 0; i < patientInfoList.Rows.Count; i++)
                {
                    // 病院登録情報の名前のスペースの位置
                    int ResultSpaceIndex = getSpaceIndex(patientInfoList.Rows[i]["PNAME"].ToString());
                    // エクセル取込情報の名前のスペースの位置
                    int LabelSpaceIndex = getSpaceIndex(NameLabel.Text);
                    
                    // 苗字のチェックボックスが入れられたら不一致のはリストに入れない
                    if (checkBoxLastName.Checked)
                    {
                        if (patientInfoList.Rows[i]["PNAME"].ToString().Substring(0, ResultSpaceIndex) != NameLabel.Text.Substring(0, LabelSpaceIndex))
                        {
                            continue;
                        }
                    }
                    // 名前のチェックボックスが入れられたら不一致のはリストに入れない
                    if (checkBoxFirstName.Checked)
                    {
                        if (patientInfoList.Rows[i]["PNAME"].ToString().Substring(ResultSpaceIndex + 1) != NameLabel.Text.Substring(LabelSpaceIndex + 1))
                        {
                            continue;
                        }
                    }
                    // 生年月日のチェックボックスが入れられたら不一致のはリストに入れない
                    if (checkBoxBirth.Checked)
                    {
                        if (patientInfoList.Rows[i]["BIRTH"].ToString().Substring(0, 10) != BirthLabel.Text)
                        {
                            continue;
                        }
                    }
                    // 電話番号のチェックボックスが入れられたら不一致のはリストに入れない
                    if (checkBoxTel.Checked)
                    {
                        if (patientInfoList.Rows[i]["JITAKTEL"].ToString() != TelLabel.Text)
                        {
                            continue;
                        }
                    }
                    dataList.Add(new PatientInfo());
                    dataList[index].KID = patientInfoList.Rows[i]["KNO"].ToString();
                    dataList[index].HospitalName = patientInfoList.Rows[i]["PNAME"].ToString();
                    dataList[index].HospitalBirth = ((DateTime)patientInfoList.Rows[i]["BIRTH"]).ToString("yyyy/MM/dd");
                    dataList[index].HospitalAddr = patientInfoList.Rows[i]["PADDR1"].ToString() + patientInfoList.Rows[i]["PADDR2"].ToString();
                    dataList[index].HospitalTel = patientInfoList.Rows[i]["JITAKTEL"].ToString();
                    index++;
                }
            }

            // 一番下になしの行を追加する
            PatientInfo blancData = new PatientInfo();
            blancData.KID = "なし";
            blancData.HospitalName = "なし";
            blancData.HospitalBirth = "なし";
            blancData.HospitalAddr = "なし";
            blancData.HospitalTel = "なし";
            dataList.Add(blancData);

            dataGridView.DataSource = dataList;
            // 病院登録情報以外は非表示
            this.dataGridView.Columns["ExcelName"].Visible = false;
            this.dataGridView.Columns["ExcelBirth"].Visible = false;
            this.dataGridView.Columns["ExcelAddr"].Visible = false;
            this.dataGridView.Columns["ExcelTel"].Visible = false;
            this.dataGridView.Columns["KID"].Visible = false;
            this.dataGridView.Columns["MatchFlg"].Visible = false;
        }
        /// <summary>
        /// 苗字と名前の間のスペースの位置の特定
        /// </summary>
        /// <param name="TargetName"></param>
        /// <returns></returns>
        public int getSpaceIndex(string TargetName)
        {
            // 半角スペースで区別
            int SpaceIndex = TargetName.IndexOf(" ");
            if (SpaceIndex < 0)
            {
                // もし半角スペースでなかったら全角スペースでも区別
                SpaceIndex = TargetName.IndexOf("　");
                if (SpaceIndex < 0)
                {
                    // それでもスペースが入っていなかったらindex0にしておく
                    return 0;
                }
            }
            return SpaceIndex;
        }
        /// <summary>
        /// 不一致項目色付け
        /// </summary>
        public void drawing ()
        {
            for (int count = 0; count < dataList.Count - 1; count++)
            {
                // 名前の不一致項目色付け
                if (!dataGridView.Rows[count].Cells["HospitalName"].Value.ToString().Equals("なし") 
                    && 
                    NameLabel.Text != dataGridView.Rows[count].Cells["HospitalName"].Value.ToString())
                {
                    dataGridView.Rows[count].Cells[1].Style.BackColor = Color.LightPink;
                }
                // 生年月日の不一致項目色付け　病院登録情報は日時を切り取って比較
                
                if (!dataGridView.Rows[count].Cells["HospitalBirth"].Value.ToString().Equals("なし") 
                    && 
                    BirthLabel.Text != dataGridView.Rows[count].Cells["HospitalBirth"].Value.ToString().Substring(0, 10))
                {
                    this.dataGridView.Rows[count].Cells[3].Style.BackColor = Color.LightPink;
                }
                // 住所の不一致項目色付け
                if (!dataGridView.Rows[count].Cells["HospitalAddr"].Value.ToString().Equals("なし") 
                    &&
                    AddrLabel.Text != dataGridView.Rows[count].Cells["HospitalAddr"].Value.ToString())
                {
                    this.dataGridView.Rows[count].Cells[7].Style.BackColor = Color.LightPink;
                }
                // 電話の不一致項目色付け
                if (!dataGridView.Rows[count].Cells["HospitalTel"].Value.ToString().Equals("なし") 
                    &&
                    TelLabel.Text != dataGridView.Rows[count].Cells["HospitalTel"].Value.ToString())
                {
                    this.dataGridView.Rows[count].Cells[5].Style.BackColor = Color.LightPink;
                }
            }
        }
        /// <summary>
        /// 苗字のチェックボックスで絞込
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxLastName_CheckedChanged(object sender, EventArgs e)
        {
            drawingList();
            drawing();
        }
        /// <summary>
        /// 名前のチェックボックスで絞込
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxFirstName_CheckedChanged(object sender, EventArgs e)
        {
            drawingList();
            drawing();
        }
        /// <summary>
        /// 生年月日のチェックボックスで絞込
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxBirth_CheckedChanged(object sender, EventArgs e)
        {
            drawingList();
            drawing();
        }
        /// <summary>
        /// 電話番号のチェックボックスで絞込
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxTel_CheckedChanged(object sender, EventArgs e)
        {
            drawingList();
            drawing();
        }
        /// <summary>
        /// フォーム読み込み時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CandidatePatientForm_Load(object sender, EventArgs e)
        {
            drawing();
        }
        /// <summary>
        /// 閉じるボタンでフォームを閉じる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {   
            matchSearch.Enabled = true;
            this.InfoLog("患者履歴一致検索画面終了(閉じるボタン)");
            this.Close();
        }
        /// <summary>
        /// ×ボタンとかで閉じられた時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CandidatePatientForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            matchSearch.Enabled = true;
            this.InfoLog("患者履歴一致検索画面終了(閉じるボタン以外)");
        }
        /// <summary>
        /// 患者様を選択してOKボタンを押したとき,表示の情報を変更してフォームを閉じる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectButton_Click(object sender, EventArgs e)
        {
            this.InfoLog("選択データの反映");
            // MatchSearchFormでの病院登録情報の表示を変更
            ((MatchSearchForm)this.Owner).changeDisplayInfo(
                 dataGridView.CurrentRow.Cells["KID"].Value.ToString(),
                dataGridView.CurrentRow.Cells["HospitalName"].Value.ToString(), 
                dataGridView.CurrentRow.Cells["HospitalBirth"].Value.ToString(), 
                dataGridView.CurrentRow.Cells["HospitalAddr"].Value.ToString(), 
                dataGridView.CurrentRow.Cells["HospitalTel"].Value.ToString());
            this.Close();
        }
    }
}
