﻿using CheckSystem.Extension;
using Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CheckSystem.MatchSearch
{
    class RKKTAccess : ILog
    {
        /// <summary>
        /// 病院情報取得
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Birth"></param>
        /// <param name="Addr"></param>
        /// <param name="Tel"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public DataTable getHospitalInfo(string Name, string Birth, string Addr, string Tel, SearchMode mode)
        {
            try
            {
                OracleDatabaseController odbc = new OracleDatabaseController();

                System.Text.StringBuilder strSQL = new System.Text.StringBuilder();

                strSQL.Append("SELECT");
                strSQL.Append("  RKKT.KNO");
                strSQL.Append(" ,RKKT.PNAME");
                strSQL.Append(" ,RKKT.BIRTH");
                strSQL.Append(" ,RKKT.PADDR1");
                strSQL.Append(" ,RKKT.PADDR2");
                strSQL.Append(" ,RKKT.JITAKTEL");
                strSQL.Append(" ,RKKTEX.YOBITEXT9");
                strSQL.Append(" FROM");
                strSQL.Append("  HN.RKKT RKKT");
                strSQL.Append(" INNER JOIN");
                strSQL.Append("  HN.RKKTEX RKKTEX");
                strSQL.Append(" ON");
                strSQL.Append("  RKKT.KNO = RKKTEX.KNO");

                // モードで取得時の条件変更
                // 0：全てが一致する時
                if (mode == SearchMode.Perfect)
                {
                    strSQL.Append(" WHERE");
                    strSQL.Append("     RKKT.PNAME = '" + Name + "' ");
                    strSQL.Append(" AND RKKT.BIRTH = '" + Birth + "' ");
                    strSQL.Append(" AND REPLACE ( REPLACE ( RKKT.PADDR1 || RKKT.PADDR2, ' ',''),'　','') =  '" + Addr + "' ");
                    strSQL.Append(" AND RKKT.JITAKTEL = " + Tel + " ");
                }
                else if (mode == SearchMode.Both)
                {
                    // 1:名前と生年月日が両方一致するやつ
                    strSQL.Append(" WHERE");
                    strSQL.Append("     RKKT.PNAME = '" + Name + "' ");
                    strSQL.Append(" AND RKKT.BIRTH = '" + Birth + "' ");
                }
                else if (mode == SearchMode.Either)
                {
                    // 1:名前（苗字と名前を分ける）と生年月日のどっちかが一致するやつ
                    int index = Name.IndexOf(" ");
                    if (index < 0)
                    {
                        index = Name.IndexOf("　");
                        if (index < 0)
                        {
                            // Nameにスペースが入っていなかったらindex0にしておく
                            index = 0;
                        }
                    }
                    // スペースより前を苗字として取得
                    string LastName = Name.Substring(0, index);
                    // スペースより後を名前として取得
                    string FirstName = Name.Substring(index + 1);
                    if (index == 0)
                    {
                        strSQL.Append(" WHERE");
                        strSQL.Append(" RKKT.PNAME LIKE '%" + FirstName + "' ");
                        strSQL.Append(" OR RKKT.BIRTH = '" + Birth + "' ");
                    }
                    else
                    {
                        strSQL.Append(" WHERE");
                        strSQL.Append("     RKKT.PNAME LIKE '" + LastName + "%' ");
                        strSQL.Append(" OR  RKKT.PNAME LIKE '%" + FirstName + "' ");
                        strSQL.Append(" OR  RKKT.BIRTH = '" + Birth + "' ");
                    }
                }

                // 実行
                return odbc.Reader(strSQL.ToString(), null); ;
            }
            catch (Exception ex)
            {
                this.ErrorLog("DB登録情報の取得に失敗しました。", ex);
                throw;
            }
        }
    }
}