﻿namespace CheckSystem.MatchSearch
{
    partial class CandidatePatientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.excelDataLabel = new System.Windows.Forms.Label();
            this.searchLabel = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.HospitalName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HospitalBirth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HospitalTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HospitalAddr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkBoxLastName = new System.Windows.Forms.CheckBox();
            this.checkBoxFirstName = new System.Windows.Forms.CheckBox();
            this.checkBoxBirth = new System.Windows.Forms.CheckBox();
            this.checkBoxTel = new System.Windows.Forms.CheckBox();
            this.NameLabel = new System.Windows.Forms.Label();
            this.CancelPatientButton = new System.Windows.Forms.Button();
            this.BirthLabel = new System.Windows.Forms.Label();
            this.TelLabel = new System.Windows.Forms.Label();
            this.AddrLabel = new System.Windows.Forms.Label();
            this.SelectButton = new System.Windows.Forms.Button();
            this.NameTitleLabel = new System.Windows.Forms.Label();
            this.BirthTitleLabel = new System.Windows.Forms.Label();
            this.TelTitleLabel = new System.Windows.Forms.Label();
            this.AddrTitleLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // excelDataLabel
            // 
            this.excelDataLabel.AutoSize = true;
            this.excelDataLabel.Location = new System.Drawing.Point(69, 22);
            this.excelDataLabel.Name = "excelDataLabel";
            this.excelDataLabel.Size = new System.Drawing.Size(90, 12);
            this.excelDataLabel.TabIndex = 0;
            this.excelDataLabel.Text = "エクセル取込情報";
            // 
            // searchLabel
            // 
            this.searchLabel.AutoSize = true;
            this.searchLabel.Location = new System.Drawing.Point(69, 98);
            this.searchLabel.Name = "searchLabel";
            this.searchLabel.Size = new System.Drawing.Size(53, 12);
            this.searchLabel.TabIndex = 1;
            this.searchLabel.Text = "検索条件";
            // 
            // dataGridView
            // 
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HospitalName,
            this.HospitalBirth,
            this.HospitalTel,
            this.HospitalAddr,
            this.KID});
            this.dataGridView.Location = new System.Drawing.Point(71, 142);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowTemplate.Height = 21;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(643, 483);
            this.dataGridView.TabIndex = 2;
            // 
            // HospitalName
            // 
            this.HospitalName.DataPropertyName = "HospitalName";
            this.HospitalName.FillWeight = 150F;
            this.HospitalName.HeaderText = "名前";
            this.HospitalName.Name = "HospitalName";
            this.HospitalName.ReadOnly = true;
            this.HospitalName.Width = 150;
            // 
            // HospitalBirth
            // 
            this.HospitalBirth.DataPropertyName = "HospitalBirth";
            this.HospitalBirth.FillWeight = 150F;
            this.HospitalBirth.HeaderText = "生年月日";
            this.HospitalBirth.Name = "HospitalBirth";
            this.HospitalBirth.ReadOnly = true;
            this.HospitalBirth.Width = 150;
            // 
            // HospitalTel
            // 
            this.HospitalTel.DataPropertyName = "HospitalTel";
            this.HospitalTel.FillWeight = 150F;
            this.HospitalTel.HeaderText = "電話番号";
            this.HospitalTel.Name = "HospitalTel";
            this.HospitalTel.ReadOnly = true;
            this.HospitalTel.Width = 150;
            // 
            // HospitalAddr
            // 
            this.HospitalAddr.DataPropertyName = "HospitalAddr";
            this.HospitalAddr.FillWeight = 150F;
            this.HospitalAddr.HeaderText = "住所";
            this.HospitalAddr.Name = "HospitalAddr";
            this.HospitalAddr.ReadOnly = true;
            this.HospitalAddr.Width = 150;
            // 
            // KID
            // 
            this.KID.DataPropertyName = "KID";
            this.KID.HeaderText = "";
            this.KID.Name = "KID";
            this.KID.ReadOnly = true;
            this.KID.Visible = false;
            // 
            // checkBoxLastName
            // 
            this.checkBoxLastName.AutoSize = true;
            this.checkBoxLastName.Location = new System.Drawing.Point(71, 113);
            this.checkBoxLastName.Name = "checkBoxLastName";
            this.checkBoxLastName.Size = new System.Drawing.Size(48, 16);
            this.checkBoxLastName.TabIndex = 3;
            this.checkBoxLastName.Text = "苗字";
            this.checkBoxLastName.UseVisualStyleBackColor = true;
            this.checkBoxLastName.CheckedChanged += new System.EventHandler(this.checkBoxLastName_CheckedChanged);
            // 
            // checkBoxFirstName
            // 
            this.checkBoxFirstName.AutoSize = true;
            this.checkBoxFirstName.Location = new System.Drawing.Point(161, 113);
            this.checkBoxFirstName.Name = "checkBoxFirstName";
            this.checkBoxFirstName.Size = new System.Drawing.Size(48, 16);
            this.checkBoxFirstName.TabIndex = 4;
            this.checkBoxFirstName.Text = "名前";
            this.checkBoxFirstName.UseVisualStyleBackColor = true;
            this.checkBoxFirstName.CheckedChanged += new System.EventHandler(this.checkBoxFirstName_CheckedChanged);
            // 
            // checkBoxBirth
            // 
            this.checkBoxBirth.AutoSize = true;
            this.checkBoxBirth.Location = new System.Drawing.Point(240, 113);
            this.checkBoxBirth.Name = "checkBoxBirth";
            this.checkBoxBirth.Size = new System.Drawing.Size(72, 16);
            this.checkBoxBirth.TabIndex = 5;
            this.checkBoxBirth.Text = "生年月日";
            this.checkBoxBirth.UseVisualStyleBackColor = true;
            this.checkBoxBirth.CheckedChanged += new System.EventHandler(this.checkBoxBirth_CheckedChanged);
            // 
            // checkBoxTel
            // 
            this.checkBoxTel.AutoSize = true;
            this.checkBoxTel.Location = new System.Drawing.Point(329, 113);
            this.checkBoxTel.Name = "checkBoxTel";
            this.checkBoxTel.Size = new System.Drawing.Size(72, 16);
            this.checkBoxTel.TabIndex = 6;
            this.checkBoxTel.Text = "電話番号";
            this.checkBoxTel.UseVisualStyleBackColor = true;
            this.checkBoxTel.CheckedChanged += new System.EventHandler(this.checkBoxTel_CheckedChanged);
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(110, 43);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(0, 12);
            this.NameLabel.TabIndex = 7;
            // 
            // CancelPatientButton
            // 
            this.CancelPatientButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelPatientButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelPatientButton.Location = new System.Drawing.Point(761, 573);
            this.CancelPatientButton.Name = "CancelPatientButton";
            this.CancelPatientButton.Size = new System.Drawing.Size(116, 52);
            this.CancelPatientButton.TabIndex = 8;
            this.CancelPatientButton.Text = "閉じる";
            this.CancelPatientButton.UseVisualStyleBackColor = true;
            this.CancelPatientButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // BirthLabel
            // 
            this.BirthLabel.AutoSize = true;
            this.BirthLabel.Location = new System.Drawing.Point(134, 64);
            this.BirthLabel.Name = "BirthLabel";
            this.BirthLabel.Size = new System.Drawing.Size(0, 12);
            this.BirthLabel.TabIndex = 9;
            // 
            // TelLabel
            // 
            this.TelLabel.AutoSize = true;
            this.TelLabel.Location = new System.Drawing.Point(297, 64);
            this.TelLabel.Name = "TelLabel";
            this.TelLabel.Size = new System.Drawing.Size(0, 12);
            this.TelLabel.TabIndex = 10;
            // 
            // AddrLabel
            // 
            this.AddrLabel.AutoSize = true;
            this.AddrLabel.Location = new System.Drawing.Point(444, 64);
            this.AddrLabel.Name = "AddrLabel";
            this.AddrLabel.Size = new System.Drawing.Size(0, 12);
            this.AddrLabel.TabIndex = 11;
            // 
            // SelectButton
            // 
            this.SelectButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SelectButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SelectButton.Location = new System.Drawing.Point(761, 488);
            this.SelectButton.Name = "SelectButton";
            this.SelectButton.Size = new System.Drawing.Size(116, 52);
            this.SelectButton.TabIndex = 12;
            this.SelectButton.Text = "OK";
            this.SelectButton.UseVisualStyleBackColor = true;
            this.SelectButton.Click += new System.EventHandler(this.SelectButton_Click);
            // 
            // NameTitleLabel
            // 
            this.NameTitleLabel.AutoSize = true;
            this.NameTitleLabel.Location = new System.Drawing.Point(69, 43);
            this.NameTitleLabel.Name = "NameTitleLabel";
            this.NameTitleLabel.Size = new System.Drawing.Size(35, 12);
            this.NameTitleLabel.TabIndex = 13;
            this.NameTitleLabel.Text = "名前：";
            // 
            // BirthTitleLabel
            // 
            this.BirthTitleLabel.AutoSize = true;
            this.BirthTitleLabel.Location = new System.Drawing.Point(69, 64);
            this.BirthTitleLabel.Name = "BirthTitleLabel";
            this.BirthTitleLabel.Size = new System.Drawing.Size(59, 12);
            this.BirthTitleLabel.TabIndex = 14;
            this.BirthTitleLabel.Text = "生年月日：";
            // 
            // TelTitleLabel
            // 
            this.TelTitleLabel.AutoSize = true;
            this.TelTitleLabel.Location = new System.Drawing.Point(238, 64);
            this.TelTitleLabel.Name = "TelTitleLabel";
            this.TelTitleLabel.Size = new System.Drawing.Size(59, 12);
            this.TelTitleLabel.TabIndex = 15;
            this.TelTitleLabel.Text = "電話番号：";
            // 
            // AddrTitleLabel
            // 
            this.AddrTitleLabel.AutoSize = true;
            this.AddrTitleLabel.Location = new System.Drawing.Point(409, 64);
            this.AddrTitleLabel.Name = "AddrTitleLabel";
            this.AddrTitleLabel.Size = new System.Drawing.Size(35, 12);
            this.AddrTitleLabel.TabIndex = 16;
            this.AddrTitleLabel.Text = "住所：";
            // 
            // CandidatePatientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(922, 637);
            this.Controls.Add(this.AddrTitleLabel);
            this.Controls.Add(this.TelTitleLabel);
            this.Controls.Add(this.BirthTitleLabel);
            this.Controls.Add(this.NameTitleLabel);
            this.Controls.Add(this.SelectButton);
            this.Controls.Add(this.AddrLabel);
            this.Controls.Add(this.TelLabel);
            this.Controls.Add(this.BirthLabel);
            this.Controls.Add(this.CancelPatientButton);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.checkBoxTel);
            this.Controls.Add(this.checkBoxBirth);
            this.Controls.Add(this.checkBoxFirstName);
            this.Controls.Add(this.checkBoxLastName);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.searchLabel);
            this.Controls.Add(this.excelDataLabel);
            this.Name = "CandidatePatientForm";
            this.Text = "患者履歴一致検索";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CandidatePatientForm_FormClosing);
            this.Load += new System.EventHandler(this.CandidatePatientForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label excelDataLabel;
        private System.Windows.Forms.Label searchLabel;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.CheckBox checkBoxLastName;
        private System.Windows.Forms.CheckBox checkBoxFirstName;
        private System.Windows.Forms.CheckBox checkBoxBirth;
        private System.Windows.Forms.CheckBox checkBoxTel;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Button CancelPatientButton;
        private System.Windows.Forms.Label BirthLabel;
        private System.Windows.Forms.Label TelLabel;
        private System.Windows.Forms.Label AddrLabel;
        private System.Windows.Forms.Button SelectButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn HospitalName;
        private System.Windows.Forms.DataGridViewTextBoxColumn HospitalBirth;
        private System.Windows.Forms.DataGridViewTextBoxColumn HospitalTel;
        private System.Windows.Forms.DataGridViewTextBoxColumn HospitalAddr;
        private System.Windows.Forms.DataGridViewTextBoxColumn KID;
        private System.Windows.Forms.Label NameTitleLabel;
        private System.Windows.Forms.Label BirthTitleLabel;
        private System.Windows.Forms.Label TelTitleLabel;
        private System.Windows.Forms.Label AddrTitleLabel;
    }
}