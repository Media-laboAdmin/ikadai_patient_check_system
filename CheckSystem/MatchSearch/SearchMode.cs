﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheckSystem.MatchSearch
{
    public enum SearchMode
    {
        //
        // 概要:
        //     DBから病院登録情報を取得する際、名前・生年月日・住所・電話番号の４項目すべてが一致する情報を取得します。
        Perfect = 0,
        //
        // 概要:
        //     DBから病院登録情報を取得する際、名前・生年月日の2項目が両方とも一致する情報を取得します。
        Both = 1,
        //
        // 概要:
        //     DBから病院登録情報を取得する際、名前・生年月日の2項目いずれか1つが一致する情報を取得します。
        Either = 2
    }
}
