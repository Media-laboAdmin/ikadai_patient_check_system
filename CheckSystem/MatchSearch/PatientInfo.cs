﻿namespace CheckSystem.MatchSearch
{
    class PatientInfo
    {
        // 名前
        public string ExcelName { get; set; }
        public string HospitalName { get; set; }

        // 生年月日
        public string ExcelBirth { get; set; }
        public string HospitalBirth { get; set; }

        // 電話番号
        public string ExcelTel { get; set; }
        public string HospitalTel { get; set; }

        // 住所
        public string ExcelAddr { get; set; }
        public string HospitalAddr { get; set; }

        // 一致不一致のボタン表示用
        public bool MatchFlg { get; set; }

        // カルテID CSV出力時に使用
        public string KID { get; set; }

    }
}
