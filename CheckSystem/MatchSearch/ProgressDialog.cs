﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CheckSystem.MatchSearch
{
    public partial class ProgressDialog : Form
    {
        /// <summary>
        /// ProgressDialogクラスのコンストラクタ
        /// </summary>
        /// <param name="caption">タイトルバーに表示するテキスト</param>
        /// <param name="doWorkHandler">バックグラウンドで実行するメソッド</param>
        /// <param name="argument">doWorkで取得できるパラメータ</param>
        public ProgressDialog(string caption,
            DoWorkEventHandler doWork)
        {
            InitializeComponent();

            //初期設定
            this.Text = caption;
            this.messageLabel.Text = "";
            this.progressBar1.Value = 0;
            this.backgroundWorker1.WorkerSupportsCancellation = true;

            //イベント
            this.Shown += new EventHandler(ProgressDialog_Shown);
            this.backgroundWorker1.DoWork += doWork;
            this.backgroundWorker1.ProgressChanged +=
                new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted +=
                new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
        }

        private Exception _error = null;
        /// <summary>
        /// バックグラウンド処理中に発生したエラー
        /// </summary>
        public Exception Error
        {
            get
            {
                return this._error;
            }
        }

        /// <summary>
        /// 進行状況ダイアログで使用しているBackgroundWorkerクラス
        /// </summary>
        public BackgroundWorker BackgroundWorker
        {
            get
            {
                return this.backgroundWorker1;
            }
        }

        //フォームが表示されたときにバックグラウンド処理を開始
        private void ProgressDialog_Shown(object sender, EventArgs e)
        {
            this.backgroundWorker1.RunWorkerAsync();
        }

        //ReportProgressメソッドが呼び出されたとき
        private void backgroundWorker1_ProgressChanged(
            object sender, ProgressChangedEventArgs e)
        {
            //プログレスバーの値を変更する
            if (e.ProgressPercentage < this.progressBar1.Minimum)
            {
                this.progressBar1.Value = this.progressBar1.Minimum;
            }
            else if (this.progressBar1.Maximum < e.ProgressPercentage)
            {
                this.progressBar1.Value = this.progressBar1.Maximum;
            }
            else
            {
                this.progressBar1.Value = e.ProgressPercentage;
            }
            //メッセージのテキストを変更する
            this.messageLabel.Text = (string)e.UserState;
        }

        //バックグラウンド処理が終了したとき
        private void backgroundWorker1_RunWorkerCompleted(
            object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(this,
                    "エラーが発生しました。\n\n" + e.Error.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                this._error = e.Error;
                this.DialogResult = DialogResult.Abort;
            }
            else if (e.Cancelled)
            {
                this.DialogResult = DialogResult.Cancel;
            }
            else
            {
                this.DialogResult = DialogResult.OK;
            }

            this.Close();
        }
    }
}