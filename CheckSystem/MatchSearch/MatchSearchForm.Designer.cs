﻿namespace CheckSystem.MatchSearch
{
    partial class MatchSearchForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.ExcelRead = new System.Windows.Forms.Button();
            this.PattarneSelect = new System.Windows.Forms.ComboBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.ExcelName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HospitalName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExcelBirth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HospitalBirth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Match = new System.Windows.Forms.DataGridViewButtonColumn();
            this.space = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExcelAddr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HospitalAddr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExcelTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HospitalTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MatchLabel = new System.Windows.Forms.Label();
            this.UnmatchLabel = new System.Windows.Forms.Label();
            this.MatchNumLabel = new System.Windows.Forms.Label();
            this.UnmatchNumLabel = new System.Windows.Forms.Label();
            this.RefineCheckBox = new System.Windows.Forms.CheckBox();
            this.outputButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ExcelRead
            // 
            this.ExcelRead.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExcelRead.Location = new System.Drawing.Point(1472, 85);
            this.ExcelRead.Name = "ExcelRead";
            this.ExcelRead.Size = new System.Drawing.Size(95, 35);
            this.ExcelRead.TabIndex = 0;
            this.ExcelRead.Text = "エクセル取込";
            this.ExcelRead.UseVisualStyleBackColor = true;
            this.ExcelRead.Click += new System.EventHandler(this.ExcelRead_Click);
            // 
            // PattarneSelect
            // 
            this.PattarneSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PattarneSelect.FormattingEnabled = true;
            this.PattarneSelect.Location = new System.Drawing.Point(1446, 48);
            this.PattarneSelect.Name = "PattarneSelect";
            this.PattarneSelect.Size = new System.Drawing.Size(121, 20);
            this.PattarneSelect.TabIndex = 1;
            this.PattarneSelect.SelectedIndexChanged += new System.EventHandler(this.PattarneSelect_SelectedIndexChanged);
            // 
            // dataGridView
            // 
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ExcelName,
            this.HospitalName,
            this.ExcelBirth,
            this.HospitalBirth,
            this.Match,
            this.space,
            this.ExcelAddr,
            this.HospitalAddr,
            this.ExcelTel,
            this.HospitalTel});
            this.dataGridView.Location = new System.Drawing.Point(55, 74);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowTemplate.Height = 21;
            this.dataGridView.Size = new System.Drawing.Size(1393, 530);
            this.dataGridView.TabIndex = 2;
            this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
            // 
            // ExcelName
            // 
            this.ExcelName.DataPropertyName = "ExcelName";
            this.ExcelName.HeaderText = "Excel取込情報";
            this.ExcelName.Name = "ExcelName";
            this.ExcelName.ReadOnly = true;
            this.ExcelName.Width = 150;
            // 
            // HospitalName
            // 
            this.HospitalName.DataPropertyName = "HospitalName";
            this.HospitalName.HeaderText = "病院登録情報";
            this.HospitalName.Name = "HospitalName";
            this.HospitalName.ReadOnly = true;
            this.HospitalName.Width = 150;
            // 
            // ExcelBirth
            // 
            this.ExcelBirth.DataPropertyName = "ExcelBirth";
            this.ExcelBirth.HeaderText = "Excel取込情報";
            this.ExcelBirth.Name = "ExcelBirth";
            this.ExcelBirth.ReadOnly = true;
            this.ExcelBirth.Width = 150;
            // 
            // HospitalBirth
            // 
            this.HospitalBirth.DataPropertyName = "HospitalBirth";
            this.HospitalBirth.HeaderText = "病院登録情報";
            this.HospitalBirth.Name = "HospitalBirth";
            this.HospitalBirth.ReadOnly = true;
            this.HospitalBirth.Width = 150;
            // 
            // Match
            // 
            this.Match.DataPropertyName = "Match";
            this.Match.HeaderText = "一致";
            this.Match.Name = "Match";
            this.Match.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Match.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Match.Width = 150;
            // 
            // space
            // 
            this.space.DataPropertyName = "space";
            this.space.HeaderText = "";
            this.space.Name = "space";
            this.space.Visible = false;
            this.space.Width = 150;
            // 
            // ExcelAddr
            // 
            this.ExcelAddr.DataPropertyName = "ExcelAddr";
            this.ExcelAddr.HeaderText = "Excel取込情報";
            this.ExcelAddr.Name = "ExcelAddr";
            this.ExcelAddr.ReadOnly = true;
            this.ExcelAddr.Width = 150;
            // 
            // HospitalAddr
            // 
            this.HospitalAddr.DataPropertyName = "HospitalAddr";
            this.HospitalAddr.HeaderText = "病院登録情報";
            this.HospitalAddr.Name = "HospitalAddr";
            this.HospitalAddr.ReadOnly = true;
            this.HospitalAddr.Width = 150;
            // 
            // ExcelTel
            // 
            this.ExcelTel.DataPropertyName = "ExcelTel";
            this.ExcelTel.HeaderText = "Excel取込情報";
            this.ExcelTel.Name = "ExcelTel";
            this.ExcelTel.ReadOnly = true;
            this.ExcelTel.Width = 150;
            // 
            // HospitalTel
            // 
            this.HospitalTel.DataPropertyName = "HospitalTel";
            this.HospitalTel.HeaderText = "病院登録情報";
            this.HospitalTel.Name = "HospitalTel";
            this.HospitalTel.ReadOnly = true;
            this.HospitalTel.Width = 150;
            // 
            // MatchLabel
            // 
            this.MatchLabel.AutoSize = true;
            this.MatchLabel.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.MatchLabel.Location = new System.Drawing.Point(59, 27);
            this.MatchLabel.Name = "MatchLabel";
            this.MatchLabel.Size = new System.Drawing.Size(147, 27);
            this.MatchLabel.TabIndex = 3;
            this.MatchLabel.Text = "患者様一致";
            // 
            // UnmatchLabel
            // 
            this.UnmatchLabel.AutoSize = true;
            this.UnmatchLabel.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.UnmatchLabel.Location = new System.Drawing.Point(495, 27);
            this.UnmatchLabel.Name = "UnmatchLabel";
            this.UnmatchLabel.Size = new System.Drawing.Size(174, 27);
            this.UnmatchLabel.TabIndex = 4;
            this.UnmatchLabel.Text = "患者様不一致";
            // 
            // MatchNumLabel
            // 
            this.MatchNumLabel.AutoSize = true;
            this.MatchNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.MatchNumLabel.Location = new System.Drawing.Point(225, 27);
            this.MatchNumLabel.Name = "MatchNumLabel";
            this.MatchNumLabel.Size = new System.Drawing.Size(0, 27);
            this.MatchNumLabel.TabIndex = 5;
            // 
            // UnmatchNumLabel
            // 
            this.UnmatchNumLabel.AutoSize = true;
            this.UnmatchNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.UnmatchNumLabel.Location = new System.Drawing.Point(689, 27);
            this.UnmatchNumLabel.Name = "UnmatchNumLabel";
            this.UnmatchNumLabel.Size = new System.Drawing.Size(0, 27);
            this.UnmatchNumLabel.TabIndex = 6;
            // 
            // RefineCheckBox
            // 
            this.RefineCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RefineCheckBox.AutoSize = true;
            this.RefineCheckBox.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.RefineCheckBox.Location = new System.Drawing.Point(1233, 49);
            this.RefineCheckBox.Name = "RefineCheckBox";
            this.RefineCheckBox.Size = new System.Drawing.Size(190, 19);
            this.RefineCheckBox.TabIndex = 7;
            this.RefineCheckBox.Text = "不一致の患者様を絞り込む";
            this.RefineCheckBox.UseVisualStyleBackColor = true;
            this.RefineCheckBox.CheckedChanged += new System.EventHandler(this.RefineCheckBox_CheckedChanged);
            // 
            // outputButton
            // 
            this.outputButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.outputButton.Location = new System.Drawing.Point(1472, 513);
            this.outputButton.Name = "outputButton";
            this.outputButton.Size = new System.Drawing.Size(95, 35);
            this.outputButton.TabIndex = 8;
            this.outputButton.Text = "CSV出力";
            this.outputButton.UseVisualStyleBackColor = true;
            this.outputButton.Click += new System.EventHandler(this.outputButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.Location = new System.Drawing.Point(1472, 569);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(95, 35);
            this.closeButton.TabIndex = 9;
            this.closeButton.Text = "閉じる";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // MatchSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1584, 621);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.outputButton);
            this.Controls.Add(this.RefineCheckBox);
            this.Controls.Add(this.UnmatchNumLabel);
            this.Controls.Add(this.MatchNumLabel);
            this.Controls.Add(this.UnmatchLabel);
            this.Controls.Add(this.MatchLabel);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.PattarneSelect);
            this.Controls.Add(this.ExcelRead);
            this.DoubleBuffered = true;
            this.Name = "MatchSearchForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "患者履歴一致検索";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ExcelRead;
        private System.Windows.Forms.ComboBox PattarneSelect;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label MatchLabel;
        private System.Windows.Forms.Label UnmatchLabel;
        private System.Windows.Forms.Label MatchNumLabel;
        private System.Windows.Forms.Label UnmatchNumLabel;
        private System.Windows.Forms.CheckBox RefineCheckBox;
        private System.Windows.Forms.Button outputButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExcelName;
        private System.Windows.Forms.DataGridViewTextBoxColumn HospitalName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExcelBirth;
        private System.Windows.Forms.DataGridViewTextBoxColumn HospitalBirth;
        private System.Windows.Forms.DataGridViewButtonColumn Match;
        private System.Windows.Forms.DataGridViewTextBoxColumn space;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExcelAddr;
        private System.Windows.Forms.DataGridViewTextBoxColumn HospitalAddr;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExcelTel;
        private System.Windows.Forms.DataGridViewTextBoxColumn HospitalTel;
    }
}

